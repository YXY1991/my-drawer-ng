import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { MinLenDirective } from "../minlen"; 


import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { SearchFormComponent } from "./serach-form.component";
import { SettingBusyComponent } from "../utils/indicator/indicator.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        SearchRoutingModule
    ],
    declarations: [
        SearchComponent,
        SearchFormComponent,
        MinLenDirective,
        SettingBusyComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SearchModule {}
