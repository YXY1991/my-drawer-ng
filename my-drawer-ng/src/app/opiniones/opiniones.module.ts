/* Add mobile styles for the component here.  */
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { OpinionesRoutingModule } from "./opiniones-routing.module";
import { OpinionesComponent } from "./opiniones.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        OpinionesRoutingModule
    ],
    declarations: [
        OpinionesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class OpinionesModule { }