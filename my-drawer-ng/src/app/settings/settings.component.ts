import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
 
let LS = require("nativescript-localstorage");

@Component({
    selector: "Settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    nombreUsuario: string;

    @Input() getNombre(){
        return this.nombreUsuario;
    }
    @Output() setNombre(x){
        this.nombreUsuario=x;
    }

    constructor() {
        // Use the component constructor to inject providers.
        this.nombreUsuario = LS.getItem('nombreUsuario');
        console.log(LS.getItem('nombreUsuario'));
    }

    doLater(fn) { setTimeout(fn, 1000); }

    ngOnInit(): void {
        /*this.doLater(() =>
            dialogs.action("Iniciar sesión?", "Cancelar!", ["Si", "No"])
                .then((result) => {
                    console.log("resultado: " + result);
                    if (result === "Si") {
                        this.doLater(() =>
                            dialogs.alert({
                                title: "Aviso ",
                                message: "Has aceptado.",
                                okButtonText: "ok"
                            }).then(() => console.log("Cerrado 1!")));
                    } else if (result === "No") {
                        this.doLater(() =>
                            dialogs
                                .alert({
                                    title: "Atención ",
                                    message: "Has cancelado.",
                                    okButtonText: "ok"
                                })
                                .then(() => console.log("Cerrado 2!"))
                        );
                    }
                }));*/
    /* 
        const toastOptions: Toast.ToastOptions = { text: "Hi there!", duration: 1000 };
        this.doLater(() => Toast.show(toastOptions));
    */

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onTapButton(e): void {
        //localStorage.removeItem ('nombreUsuario');
        LS.setItem('nombreUsuario', this.getNombre());
        console.log(LS.getItem('nombreUsuario').toUpperCase());
        const toastOptions: Toast.ToastOptions = { text: "Hola " + this.nombreUsuario, duration: 1000 };
        this.doLater(() => Toast.show(toastOptions));
    }
}
