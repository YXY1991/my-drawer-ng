import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyFriendsComponent } from "./my-friends.component";

const routes: Routes = [
    { path: "", component: MyFriendsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyFriendsRoutingModule { }
