import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { FeaturedComponent } from "./featured.component";
import { Featured1Component } from "../featured1/featured1.component";
import { Featured2Component } from "../featured2/featured2.component";

const routes: Routes = [
    { path: "", component: FeaturedComponent },
    { path: "featured1", component: Featured1Component },
    { path: "featured2", component: Featured2Component }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FeaturedRoutingModule { }
