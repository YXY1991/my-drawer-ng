import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MyFriendsRoutingModule } from "./my-friends-routing.module";
import { MyFriendsComponent } from "./my-friends.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MyFriendsRoutingModule
    ],
    declarations: [
        MyFriendsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MyFriendsModule { }